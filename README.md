### Box To Onbase ###

* Takes files uploaded on formstack to box and puts them in onbase share.
* 0.1

### How do I get set up? ###

Requires

* Python3
  * pysftp, re, os, csv 
* rsa key for boxtosftp account

### Contribution guidelines ###

* variables are in sftpConfig.py

### Who do I talk to? ###

* Eduardo Figueroa - sftp, python, box
* Pierre Ioga - Formstack
* Jason Ferguson - Onbase