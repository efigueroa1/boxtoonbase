import csv
#create CSV for onbase ingestion
def createCSV(name,downloadDir):
    parts = name.split('__')
    parts.append(name)
    csvName         = downloadDir + name + '.csv'
    with open(csvName,'w',newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
        writer.writerow(parts[:])
    return csvName



