import pysftp, re, os
from sftpConfig import *
from onbase import *

print("state=starting")

allowedFile = re.compile('^[0-9]',re.IGNORECASE)

# Make changes in the config.py
sftp = pysftp.Connection('sftp.csub.edu', username=sftpUser, private_key=sftpKey)

# get files
files = sftp.listdir(remoteDir)

for file in files:
    name = file.rstrip('\x00')
    filePath = remoteDir+name
    if len(name) > 3:
        if allowedFile.match(name) and sftp.isfile(filePath):
            localFile = downloadDir+name
            destFile = destDir+name
            #download it from box
            sftp.get(filePath,localFile, preserve_mtime=True)
            csvFile = createCSV(name,downloadDir)
            #upload it to onbase
            with sftp.cd(destDir):
                sftp.put(localFile)
                print("upload="+localFile)
                sftp.put(csvFile)
                print("upload="+csvFile)
            #delete it from local drive and box folder
            os.remove(localFile)
            os.remove(csvFile)
            sftp.remove(filePath)

print("state=stopping")
sftp.close()
